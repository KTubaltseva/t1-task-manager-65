package ru.t1.ktubaltseva.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.ktubaltseva.tm.client.TaskRestEndpointClient;
import ru.t1.ktubaltseva.tm.client.TasksRestEndpointClient;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.marker.IntegrationCategory;
import ru.t1.ktubaltseva.tm.model.Task;

import java.util.Collection;

import static ru.t1.ktubaltseva.tm.constant.TaskTestData.*;

@Category(IntegrationCategory.class)
public final class TaskEndpointTest extends AbstractTest {

    @NotNull
    private final TaskRestEndpointClient modelEndpointClient = TaskRestEndpointClient.client();

    @NotNull
    private final TasksRestEndpointClient modelsEndpointClient = TasksRestEndpointClient.client();

    @BeforeClass
    @SneakyThrows
    public static void beforeClazz() {
    }

    @AfterClass
    @SneakyThrows
    public static void afterClazz() {
    }

    @After
    @SneakyThrows
    public void after() {
        if (modelEndpointClient.existsById(MODEL_1.getId()))
            modelEndpointClient.deleteById(MODEL_1.getId());
    }

    @Test
    public void create() throws AbstractException {
        @Nullable final Task modelToAdd = MODEL_1;
        @Nullable final String modelToAddId = modelToAdd.getId();

        @Nullable final Task modelAdded = modelEndpointClient.create(modelToAdd);
        Assert.assertNotNull(modelAdded);
        Assert.assertEquals(modelToAdd.getId(), modelAdded.getId());

        @Nullable final Task modelFindOneById = modelEndpointClient.findById(modelToAddId);
        Assert.assertNotNull(modelFindOneById);
        Assert.assertEquals(modelToAdd.getId(), modelFindOneById.getId());
    }

    @Test
    public void findById() throws AbstractException {
        @NotNull final Task modelExists = MODEL_1;
        modelEndpointClient.create(modelExists);

        @Nullable final Task modelFindOneById = modelEndpointClient.findById(modelExists.getId());
        Assert.assertNotNull(modelFindOneById);
        Assert.assertEquals(modelExists.getId(), modelFindOneById.getId());
    }


    @Test
    public void findAll() throws AbstractException {
        @NotNull final Task modelExists = MODEL_1;
        modelEndpointClient.create(modelExists);

        @NotNull final Collection<Task> modelsFindAllNoEmpty = modelsEndpointClient.findAll();
        Assert.assertNotNull(modelsFindAllNoEmpty);
    }

    @Test
    @Ignore
    public void clear() {
        modelsEndpointClient.clear();
        Assert.assertEquals(0, modelsEndpointClient.count());
    }

    @Test
    public void deleteById() throws AbstractException {
        @Nullable final Task modelToRemove = MODEL_1;
        modelEndpointClient.create((modelToRemove));

        modelEndpointClient.deleteById(modelToRemove.getId());

        Assert.assertFalse(modelEndpointClient.existsById(modelToRemove.getId()));
    }

    @Test
    public void existsById() throws AbstractException {
        @NotNull final Task modelExists = MODEL_1;
        modelEndpointClient.create(modelExists);

        Assert.assertFalse(modelEndpointClient.existsById(NON_EXISTENT_MODEL_ID));
        Assert.assertTrue(modelEndpointClient.existsById(modelExists.getId()));
    }

    @Test
    public void updateTaskById() throws AbstractException {
        @Nullable final Task modelToUpdate = modelEndpointClient.create((MODEL_1));
        modelToUpdate.setName(MODEL_NAME);

        @Nullable final Task modelUpdated = modelEndpointClient.update(modelToUpdate);
        Assert.assertNotNull(modelUpdated);
        Assert.assertEquals(modelUpdated.getId(), modelToUpdate.getId());
        Assert.assertEquals(modelToUpdate.getName(), modelUpdated.getName());
    }

}
