package ru.t1.ktubaltseva.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.ktubaltseva.tm.api.endpoint.IProjectsEndpoint;
import ru.t1.ktubaltseva.tm.api.service.model.IProjectService;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.model.Project;

import java.util.List;

@RestController
@RequestMapping("/api/projects")
public final class ProjectsEndpoint implements IProjectsEndpoint {

    @Autowired
    private IProjectService service;

    @NotNull
    @Override
    @GetMapping("/findAll")
    public List<Project> findAll() {
        return service.findAll();
    }

    @Override
    @DeleteMapping("/clear")
    public void clear() {
        service.clear();
    }

    @Override
    @GetMapping("/count")
    public long count() {
        return service.count();
    }

}

