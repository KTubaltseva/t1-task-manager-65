package ru.t1.ktubaltseva.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.ktubaltseva.tm.api.endpoint.ITaskEndpoint;
import ru.t1.ktubaltseva.tm.api.service.model.ITaskService;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.model.Project;
import ru.t1.ktubaltseva.tm.model.Task;

@RestController
@RequestMapping("/api/task")
public final class TaskEndpoint implements ITaskEndpoint {

    @Autowired
    private ITaskService service;

    @NotNull
    @Override
    @PutMapping("/create")
    public Task create() throws AbstractException {
        return service.create();
    }

    @NotNull
    @Override
    @PutMapping("/add")
    public Task create(@RequestBody @NotNull final Task task) throws AbstractException {
        return service.create(task);
    }

    @Override
    @DeleteMapping("/delete/{id}")
    public void deleteById(@PathVariable("id") @NotNull final String id) throws AbstractException {
        service.deleteById(id);
    }

    @Override
    @GetMapping("/existsById/{id}")
    public boolean existsById(@PathVariable("id") @NotNull final String id) throws AbstractException {
        return service.existsById(id);
    }

    @NotNull
    @Override
    @GetMapping("/findById/{id}")
    public Task findById(@PathVariable("id") @NotNull final String id) throws AbstractException {
        return service.findById(id);
    }

    @NotNull
    @Override
    @PostMapping("/update/{id}")
    public Task update(@RequestBody @NotNull final Task task) throws AbstractException {
        return service.update(task);
    }

}

