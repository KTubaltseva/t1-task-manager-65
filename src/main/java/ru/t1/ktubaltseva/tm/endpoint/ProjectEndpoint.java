package ru.t1.ktubaltseva.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.ktubaltseva.tm.api.endpoint.IProjectEndpoint;
import ru.t1.ktubaltseva.tm.api.service.model.IProjectService;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.model.Project;

@RestController
@RequestMapping("/api/project")
public final class ProjectEndpoint implements IProjectEndpoint {

    @Autowired
    private IProjectService service;

    @NotNull
    @Override
    @PutMapping("/create")
    public Project create() throws AbstractException {
        return service.create();
    }

    @NotNull
    @Override
    @PutMapping("/add")
    public Project create(@RequestBody @NotNull final Project project) throws AbstractException {
        return service.create(project);
    }

    @Override
    @DeleteMapping("/delete/{id}")
    public void deleteById(@PathVariable("id") @NotNull final String id) throws AbstractException {
        service.deleteById(id);
    }

    @Override
    @GetMapping("/existsById/{id}")
    public boolean existsById(@PathVariable("id") @NotNull final String id) throws AbstractException {
        return service.existsById(id);
    }

    @NotNull
    @Override
    @GetMapping("/findById/{id}")
    public Project findById(@PathVariable("id") @NotNull final String id) throws AbstractException {
        return service.findById(id);
    }

    @NotNull
    @Override
    @PostMapping("/update/{id}")
    public Project update(@RequestBody @NotNull final Project project) throws AbstractException {
        return service.update(project);
    }

}

