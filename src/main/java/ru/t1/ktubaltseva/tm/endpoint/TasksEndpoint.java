package ru.t1.ktubaltseva.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.t1.ktubaltseva.tm.api.endpoint.ITasksEndpoint;
import ru.t1.ktubaltseva.tm.api.service.model.ITaskService;
import ru.t1.ktubaltseva.tm.model.Task;

import java.util.List;

@RestController
@RequestMapping("/api/tasks")
public final class TasksEndpoint implements ITasksEndpoint {

    @Autowired
    private ITaskService service;

    @NotNull
    @Override
    @GetMapping("/findAll")
    public List<Task> findAll() {
        return service.findAll();
    }


    @Override
    @DeleteMapping("/clear")
    public void clear() {
        service.clear();
    }

    @Override
    @GetMapping("/count")
    public long count() {
        return service.count();
    }

}

