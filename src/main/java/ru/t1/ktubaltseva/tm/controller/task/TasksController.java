package ru.t1.ktubaltseva.tm.controller.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.ktubaltseva.tm.api.service.model.IProjectService;
import ru.t1.ktubaltseva.tm.api.service.model.ITaskService;
import ru.t1.ktubaltseva.tm.repository.model.ProjectRepository;
import ru.t1.ktubaltseva.tm.repository.model.TaskRepository;

@Controller
public class TasksController {

    @Autowired
    private ITaskService taskService;

    @Autowired
    private IProjectService projectService;

    @GetMapping("/tasks")
    public ModelAndView index() {
        @NotNull final ModelAndView modelAndView = new ModelAndView("task/task-list");
        modelAndView.addObject("tasks", taskService.findAll());
        modelAndView.addObject("projectService", projectService);
        return modelAndView;
    }

}

