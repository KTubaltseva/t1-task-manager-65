package ru.t1.ktubaltseva.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "tm_task")
public final class Task extends AbstractModel {

    @Nullable
    @Column(name = "project_id")
    private String projectId;

    public Task(@Nullable String name) {
        super(name);
    }


    public Task() {
        super();
    }

}
