package ru.t1.ktubaltseva.tm.service;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.t1.ktubaltseva.tm.api.service.IPropertyService;

@Getter
@Service
@PropertySource("classpath:application.properties")
public class PropertyService implements IPropertyService {

    @NotNull
    @Value("#{environment['password.iteration']}")
    public Integer passwordIteration;

    @NotNull
    @Value("#{environment['password.secret']}")
    public String passwordSecret;

    @NotNull
    @Value("#{environment['server.port']}")
    public Integer serverPort;

    @NotNull
    @Value("#{environment['server.host']}")
    public String serverHost;

    @NotNull
    @Value("#{environment['session.timeout']}")
    public Integer sessionTimeout;

    @NotNull
    @Value("#{environment['session.key']}")
    public String sessionKey;

    @NotNull
    @Value("#{environment['database.username']}")
    public String databaseUsername;

    @NotNull
    @Value("#{environment['database.password']}")
    public String databasePassword;

    @NotNull
    @Value("#{environment['database.url']}")
    public String databaseURL;

    @NotNull
    @Value("#{environment['database.driver']}")
    public String databaseDriver;

    @NotNull
    @Value("#{environment['database.dialect']}")
    public String databaseDialect;

    @NotNull
    @Value("#{environment['database.hbm2ddl']}")
    public String databaseHBM2DDL;

    @NotNull
    @Value("#{environment['database.showSql']}")
    public String databaseShowSql;

    @NotNull
    @Value("#{environment['database.second_lvl_cache']}")
    public String databaseSecondLvlCache;

    @NotNull
    @Value("#{environment['database.factory_class']}")
    public String databaseFactoryClass;

    @NotNull
    @Value("#{environment['database.use_query_cache']}")
    public String databaseUseQueryCache;

    @NotNull
    @Value("#{environment['database.use_min_puts']}")
    public String databaseUseMinPuts;

    @NotNull
    @Value("#{environment['database.region_prefix']}")
    public String databaseRegionPrefix;

    @NotNull
    @Value("#{environment['database.config_file_path']}")
    public String databaseConfigFilePath;

    @NotNull
    @Value("#{environment['database.init_token']}")
    public String databaseInitToken;

}
