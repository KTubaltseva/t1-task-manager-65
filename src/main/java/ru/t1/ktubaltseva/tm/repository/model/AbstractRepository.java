package ru.t1.ktubaltseva.tm.repository.model;

import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.ktubaltseva.tm.model.AbstractModel;

@Repository
@Scope("prototype")
public interface AbstractRepository<M extends AbstractModel> extends JpaRepository<M, String> {

}
