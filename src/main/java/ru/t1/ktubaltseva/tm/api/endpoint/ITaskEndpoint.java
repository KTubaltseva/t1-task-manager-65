package ru.t1.ktubaltseva.tm.api.endpoint;

import ru.t1.ktubaltseva.tm.model.Task;

public interface ITaskEndpoint extends IModelEndpoint<Task> {

}
