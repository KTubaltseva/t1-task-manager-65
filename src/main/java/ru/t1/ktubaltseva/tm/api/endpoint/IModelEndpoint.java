package ru.t1.ktubaltseva.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.model.AbstractModel;

public interface IModelEndpoint<M extends AbstractModel> {

    @NotNull
    M create() throws AbstractException;

    @NotNull
    M create(@NotNull M model) throws AbstractException;

    void deleteById(@NotNull String id) throws AbstractException;

    boolean existsById(@NotNull String id) throws AbstractException;

    @NotNull
    M findById(@NotNull String id) throws AbstractException;

    @NotNull
    M update(@NotNull M model) throws AbstractException;

}
