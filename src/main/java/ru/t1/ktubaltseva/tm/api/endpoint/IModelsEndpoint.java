package ru.t1.ktubaltseva.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.model.AbstractModel;
import ru.t1.ktubaltseva.tm.model.Project;

import java.util.List;

public interface IModelsEndpoint<M extends AbstractModel> {

    @NotNull
    List<M> findAll() ;

    void clear();

    long count();

}
